<?php

header("Content-type: text/json; charset=utf-8");

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '1234');
define('DB_NAME', 'json_example');

$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die('Error connecting to mysql');

mysql_select_db( DB_NAME );
mysql_query("SET NAMES UTF8");

$rs = mysql_query("SELECT * FROM products WHERE 1=1 ORDER BY id");

$results = array();
while( $row = mysql_fetch_array($rs) ){
	$results[] = $row;
}

echo json_format(json_encode($results));


	/* Json 格式化
	 * 
	 * Params:
	 *			*json(string) => JSON字串
	 * Note:
	 *			顯示出來的JSON字串會成為可視化
	 */
	function json_format($json)	{
		$tab = "  ";
		$new_json = "";
		$indent_level = 0;
		$in_string = false;
		$json_obj = json_decode($json);
		if(!$json_obj){
			return false;
		}
		$json = json_encode($json_obj);
		$len = strlen($json);
		for($c = 0; $c < $len; $c++) {
			$char = $json[$c];
			switch($char) {
				case '{':
				case '[':
					if(!$in_string) {
						$new_json .= $char . "\n" . str_repeat($tab, $indent_level+1);
						$indent_level++;
					} else {
						$new_json .= $char;
					}
				break;
				case '}':
				case ']':
					if(!$in_string){
						$indent_level--;
						$new_json .= "\n" . str_repeat($tab, $indent_level) . $char;
					} else {
						$new_json .= $char;
					}
				break;
				case ',':
					if(!$in_string){
						$new_json .= ",\n" . str_repeat($tab, $indent_level);
					} else {
						$new_json .= $char;
					}
				break;
				case ':':
					if(!$in_string) {
						$new_json .= ": ";
					} else {
						$new_json .= $char;
					}
				break;
				case '"':
					$in_string = !$in_string;
				default:
					$new_json .= $char;
				break;
			}
		}
		return $new_json;
	}

?>